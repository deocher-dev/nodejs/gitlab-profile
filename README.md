# Projects of `@noscope`

The following projects are `npm` packages intended for use in real Node.js
applications. Some of them are not battle-tested and thus marked as
**experimental**.

- [@noscope/config-manager](https://gitlab.com/deocher-dev/nodejs/noscope/config-manager) -
  a configuration manager for Node.js applications

# Educational Projects

There are a couple of educational projects intended to investigate features and
mechanics of Node.js.

The following projects are `npm` packages with tests that either reproduce some
specific scenarios or demonstrate typical use cases of particular features.

- [demo-event-loop](https://gitlab.com/deocher-dev/nodejs/demo-event-loop) -
  investigation of the Event Loop
- [demo-node-stream](https://gitlab.com/deocher-dev/nodejs/demo-node-stream) -
  implementation of custom readable and writable streams
